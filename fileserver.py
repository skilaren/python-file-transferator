import socket
import os.path
from threading import Thread

clients = []


# Thread to listen one particular client
class ClientListener(Thread):
    def __init__(self, name: str, sock: socket.socket):
        super().__init__(daemon=True)
        self.sock = sock
        self.name = name

    # clean up
    def _close(self):
        clients.remove(self.sock)
        self.sock.close()
        print(self.name + ' disconnected')

    def _get_new_name(self, filename):
        cnt = 0
        # if there is no file with same name, return original
        if not os.path.isfile(filename):
            return filename
        # if there is then create new name from template <filename>_copy<number>.<file_extension>
        else:
            filename, file_extension = filename.split('.')
            while True:
                cnt += 1
                new_filename = f'{filename}_copy{cnt}.{file_extension}'
                if not os.path.isfile(new_filename):
                    return new_filename

    def run(self):
        while True:
            # read 4 bytes which is the length of file name
            filename_len = int(self.sock.recv(4).decode('ascii'))
            # read file name
            filename = self.sock.recv(filename_len).decode('ascii')
            # read length of content
            content_len = int(self.sock.recv(10).decode('ascii'))
            print(filename_len)
            print(filename)
            print(content_len)
            if filename_len and content_len:
                file = open(self._get_new_name(filename), 'wb')
                accepted_len = 0
                while True:
                    # receive content of file
                    content = self.sock.recv(1024)
                    if content:
                        file.write(content)
                        accepted_len += 1024
                    else:
                        file.close()
                        if accepted_len >= content_len:
                            print('file saved successfully')
                        else:
                            print('transfer was corrupted, file is not full')
                        self._close()
                        break
            else:
                # if we got no data – client has disconnected
                self._close()
                # finish the thread
                break
        return


if __name__ == '__main__':

    clients_cnt = 1

    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    sock.bind(('', 8800))
    sock.listen()
    while True:
        con, addr = sock.accept()
        clients.append(con)
        name = f'client{clients_cnt}'
        clients_cnt += 1
        ClientListener(name, con).start()
