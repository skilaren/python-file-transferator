import sys
import socket
from os import path


# print progress of sending
def show_progress(sent_size, full_size):
    if sent_size % 10240 == 0:
        print(f'Completed: {sent_size/full_size}%')
        return
    if sent_size >= full_size:
        print(f'Completed: 100%')
        return


if __name__ == '__main__':
    if len(sys.argv) < 4:
        print('You are missing arguments, received: ' + str(len(sys.argv) - 1))
    else:
        # get values from CL args
        filename = sys.argv[1]
        server_ip = sys.argv[2]
        server_port = sys.argv[3]

        # create socket and send file name length, file name and content length
        sock = socket.socket()
        sock.connect((server_ip, int(server_port)))

        sock.send(str(len(filename)).encode('ascii').zfill(4))
        sock.send(filename.encode('ascii'))

        file_size = path.getsize(path.abspath(filename))
        sock.send(str(file_size).encode('ascii').zfill(10))

        # send content of file
        with open(filename, 'rb') as file:
            sent_size = 0
            while True:
                data = file.read(1024)
                if data:
                    sock.send(data)
                    sent_size += 1024
                    show_progress(sent_size, file_size)
                else:
                    break
